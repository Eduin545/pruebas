*** Settings ***
Library  SeleniumLibrary
*** Test Cases ***
Entrar a la pagina
    Open browser    http://localhost/pagina1/   Chrome
    Sleep  2s
Entrar al login
    Click element  xpath://*[@id="bs-example-navbar-collapse-1"]/ul/li[4]/a
    Sleep  3s
Ingresar como administrador
    input text  xpath://*[@id="modalLog"]/div/div/form/div[1]/input   felipe
    input text  xpath://*[@id="modalLog"]/div/div/form/div[2]/input   123
    Click element  xpath://*[@id="modalLog"]/div/div/form/div[4]/label/input
    Click element  xpath://*[@id="modalLog"]/div/div/form/div[5]/button[1]
    Sleep  2s
Salir de la cuenta
    Click element  xpath://*[@id="bs-example-navbar-collapse-1"]/ul[1]/li/a
    Click element  xpath://*[@id="bs-example-navbar-collapse-1"]/ul[1]/li/ul/li[6]/a
    Sleep  3s
    Close browser