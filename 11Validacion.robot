*** Settings ***
Library  SeleniumLibrary
*** Test Cases ***
Entrar a la pagina
    Open browser    http://localhost/pagina1/   Chrome
    Sleep  2s
Pruebas registro
    Click element  xpath://*[@id="bs-example-navbar-collapse-1"]/ul/li[3]/a
    Sleep  2s
Ingresar datos
    input text  xpath:/html/body/div[3]/div/div/div/div[2]/form/div[1]/input   Daniel Alzate Pulgarin
    input text  xpath://*[@id="input_user"]   MotorwebUs1
    input text  xpath:/html/body/div[3]/div/div/div/div[2]/form/div[3]/input   contrasena
    input text  xpath:/html/body/div[3]/div/div/div/div[2]/form/div[4]/input   daniel@elpoli.edu.co
Crear usuario
    Click element  xpath:/html/body/div[3]/div/div/div/div[2]/form/button
    Sleep  3s
    Close browser