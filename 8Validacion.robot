*** Settings ***
Library  SeleniumLibrary
*** Test Cases ***
Entrar a la pagina
    Open browser    http://localhost/pagina1/   Chrome
    Sleep  2s
Entrar al login
    Click element  xpath://*[@id="bs-example-navbar-collapse-1"]/ul/li[4]/a
    Sleep  2s
Ingresar como usuario
    input text  xpath://*[@id="modalLog"]/div/div/form/div[1]/input   MotorwebUsuario
    input text  xpath://*[@id="modalLog"]/div/div/form/div[2]/input   contrasena
    Click element  xpath://*[@id="modalLog"]/div/div/form/div[5]/button[1]
    Sleep  2s
Ingresar a las reservas
    Click element  xpath://*[@id="bs-example-navbar-collapse-1"]/ul[2]/li[2]/a
    Click element  xpath:/html/body/div[3]/div[2]/div/div/div[2]/a
    Sleep  2s
Ingresar datos
    select from list by label  name_ticket   Discover 125
    select from list by label  email_ticket   4 Tiempos
    select from list by label  departamento_ticket   Usada
    select from list by label  asunto_ticket   Reserva
    input text  xpath:/html/body/div[3]/div[2]/div/div/div[2]/div/div[2]/form/fieldset/div[6]/div/textarea   Automatica de 2 Tiempos
Hacer reserva
    Click element  xpath:/html/body/div[3]/div[2]/div/div/div[2]/div/div[2]/form/fieldset/div[7]/div/button
    Sleep   3s
    Close browser