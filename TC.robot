*** Settings ***
Library  SeleniumLibrary
*** Variables ***

*** Test Cases ***
Entrar a la pagina
    Open browser    http://localhost/pagina1/   Chrome
    Sleep  2s
Pruebas registro
    Click element  xpath://*[@id="bs-example-navbar-collapse-1"]/ul/li[3]/a
    Sleep  2s
Ingresar datos
    Set Global Variable     @{Imput}    1   2   3
    :FOR    ${INDEX}   IN      @{Imput}
        \   input text  name:nom_complete_reg   Daniel Alzate
        \   input text  name:user_reg   MWU${INDEX}
        \   input text  name:clave_reg   contrasena
        \   input text  name:email_reg   danielalzate${INDEX}@elpoli.edu.co
        \   Click element  name:btn_reg
        \   Sleep  5s
    Close browser